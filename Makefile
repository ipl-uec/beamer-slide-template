# build settings

RM := rm -rf

LATEXMK := latexmk
EXTRACTBB := extractbb

# source files

TARGET ?= main.pdf
TARGET_HANDOUT ?= $(TARGET:.pdf=_handout.pdf)

MAIN_SOURCE := $(TARGET:.pdf=.tex)
MAIN_HANDOUT_SOURCE := $(TARGET_HANDOUT:.pdf=.tex)

# main tasks

.SUFFIXES: .tex .dvi .pdf

.PHONY: all
all: build

.PHONY: build
build: $(TARGET) $(TARGET_HANDOUT)


# tasks
.PHONY: extract_images
extract_images:
	$(EXTRACTBB) img/*png

.PHONY: $(TARGET) $(TARGET_HANDOUT)
$(TARGET): $(MAIN_SOURCE) extract_images
	$(LATEXMK) -pdfdvi $<

$(TARGET_HANDOUT): $(MAIN_HANDOUT_SOURCE) extract_images
	$(LATEXMK) -pdfdvi $<

$(MAIN_HANDOUT_SOURCE): $(MAIN_SOURCE)
	sed -e 's/\(\documentclass\[.*\)\]/\1,handout]/' $< > $@

.tex.pdf: extract_images
	$(LATEXMK) -pdfdvi $<


.PHONY: preview
preview:
	$(LATEXMK) -pdfdvi -pv $(MAIN_SOURCE)

.PHONY: watch watch-without-prev
watch:
	$(LATEXMK) -pdfdvi -pvc $(MAIN_SOURCE)

watch-without-prev:
	$(LATEXMK) -pdfdvi -pvc -view=none $(MAIN_SOURCE)

.PHONY: clean remove

clean: lessclean
	find . -name '*.snm' -delete
	find . -name '*.fls' -delete
	find . -name '*.*pk' -delete
	find . -name '*.tfm' -delete
	find . -name '*.tmp' -delete

lessclean:
	latexmk -c -bibtex
	find . -name '*.aux' -delete
	find . -name '*.synctex.gz*' -delete
	find . -name '*.dvi' -delete
	find . -name '*.nav' -delete
	find . -name '*.vrb' -delete
	$(RM) $(MAIN_HANDOUT_SOURCE)

remove: clean
	$(RM) $(TARGET)
	$(RM) $(TARGET_HANDOUT)
