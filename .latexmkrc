#!/usr/bin/env perl

use v5.10;
use experimental qw(smartmatch);

# envs
$ENV{'TEXINPUTS'}     = './sty//:./fonts//:' . ($ENV{'TEXINPUTS'} // '');
$ENV{'TEXMFCNF'}      = '.:'         . ($ENV{'TEXMFCNF'}    // '');
$ENV{'TEXFONTMAPS'}   = './fonts//:' . ($ENV{'TEXFONTMAPS'} // '');
$ENV{'BSTINPUTS'}     = './sty//:'   . `kpsewhich -show-path=bst`;
$ENV{'MFINPUTS'}      = './fonts//:' . `kpsewhich -show-path=mf`;
$ENV{'TTFONTS'}       = './fonts//:' . `kpsewhich -show-path='truetype fonts'`;
$ENV{'OPENTYPEFONTS'} = './fonts//:' . `kpsewhich -show-path='opentype fonts'`;
$ENV{'VFFONTS'}       = './fonts//:' . `kpsewhich -show-path=vf`;
$ENV{'T1FONTS'}       = './fonts//:' . `kpsewhich -show-path='type1 fonts'`;
$ENV{'PKFONTS'}       = './fonts//:' . `kpsewhich -show-path=pk`;
$ENV{'AFMFONTS'}      = './fonts//:' . `kpsewhich -show-path=afm`;

# tex options
$latex        = 'platex -synctex=1 -shell-escape -halt-on-error -kanji=utf8';
$latex_silent = $latex . ' -interaction=batchmode';
$biber        = 'biber %O --bblencoding=utf8 -u -U --output_safechars %B';
$bibtex       = 'pbibtex %O %B';
$dvipdf       = 'dvipdfmx -d 5 -f font.map %O -o %D %S';
$makeindex    = 'mendex %O -o %D %S';
$max_repeat   = 5;
$pdf_modei    = 3;

# default preview
given ($^O) {
  when (/MSWin32/) {
    # do nothing
  }
  when (/darwin/) {
    $pvc_view_file_via_temporary = 0;
    $pdf_previewer = 'open -ga /Applications/Skim.app';
  }
  default {
    $pdf_previewer = 'evince';
  }
}

# local config
$local_latexmkrc_path = './.latexmkrc.local';
require $local_latexmkrc_path if -e $local_latexmkrc_path;
