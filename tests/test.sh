#!/usr/bin/env bash

set -e
[[ "$DEBUG" = "true" ]] && set -x

script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

PATH="$(dirname $script_dir)/bin:${PATH}"

rm -rf tests/test-slide
newslide tests/test-slide

cd tests/test-slide
git status
make

