# Beamer Slide Template

## Create new slide package

```bash
newslide [slide dir]
```

See also:

```bash
newslide --help
```

## Installation

```bash
make
```

or using `docker-compose`

### Dependencies

* pLaTeX (and some japanese fonts)
* LatexMk
* findutils (for clean task)
* git supports (optional: See https://git-scm.com/)
* editorconfig supports (optional: See http://editorconfig.org)
* python and Pygments (optional: for minted package)

## Watching and Auto Building

```
make watch
```

## How to write slides?

1. Make article file in `arts/`
1. Write beamer frames
1. run `make` and preview `main.pdf`

Also see [arts/sample.tex](arts/sample.tex) and `*.tex` files.

## How to customize?

TODO
